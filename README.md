# Environment Variables

A simple Java Spring application, that utilises environment variables, in order to be configured.

# Good practices

Keep a `dev.env` file with default values or settings that have no problem finding their way into source control (e.g. a default port).

Leave secrets undefined  and explicitly mention that they should be initialized in another file, leaving the `dev.env` untouched.
Add any other than the `dev.env` to the `.gitgnore`. 

This way you will have better chances into preventing accidentally commits containing sensitive information.

Another benefit of this approach is that it makes pretty clear what settings you are expected to modify.

# Tips for IntelliJ

If using this IDE, install the [EnvFile](https://plugins.jetbrains.com/plugin/7861-envfile) plugin because it will be very handy to set and override variables using `.env` files. It is also convenient to install the [.env files support](https://plugins.jetbrains.com/plugin/9525--env-files-support) plugin.

Make sure you also install the [Lombok](https://plugins.jetbrains.com/plugin/6317-lombok) plugin to ensure proper IntelliSense when using Lombok annotations.