package com.ainacio.environmentvariables;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SomeService {

    @Value("${GREETING}")
    private String greeting;

    @Value("${GREETINGS_TO}")
    private String greetingsTo;

    @Scheduled(fixedDelay=5000)
    public void saySomething() {
      log.info("{} {}", greeting, greetingsTo);
    }

}
