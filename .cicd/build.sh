#!/bin/bash

#Exit immediately if a command exits with a non-zero status.
set -e

cd $(dirname "$0")

echo Running for $CI_ENVIRONMENT_NAME
echo Version $CI_PIPELINE_IID

source config.sh

echo "================================================================================"
echo "Building..."
echo "================================================================================"
echo build $IMAGE_TAG_BASE
docker build -t $IMAGE_TAG_BASE -f $APPLICATION_DOCKERFILE $IMAGE_BASE_FOLDER
echo "================================================================================"